# Stage 0, "Build-stage", based on Node.js, to build and compile the frontend
FROM node:10.19-buster as build-stage

# Change to the working directory and copy package.json files
WORKDIR /app
COPY package*.json /app/

#install npm packages and copy app
RUN npm install
RUN npm install @angular/cli@9.0.6 -g

COPY . /app/

RUN ng build --prod --output-path=./dist/out


# Stage 1, based on Nginx, to have only the compiled app, ready for production with nginx
FROM nginx:1.17

COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html

# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY nginx.conf /etc/nginx/conf.d/default.conf
