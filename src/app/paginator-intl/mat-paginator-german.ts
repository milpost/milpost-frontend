import { MatPaginatorIntl } from '@angular/material/paginator';

export function matPaginatorGerman() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.nextPageLabel = 'Nächste Seite';
  paginatorIntl.previousPageLabel = 'Vorherige Seite';
  paginatorIntl.lastPageLabel = 'Letzte Seite';
  paginatorIntl.firstPageLabel = 'Erste Seite';
  paginatorIntl.itemsPerPageLabel = 'Einträge pro Seite';

  return paginatorIntl;
}
