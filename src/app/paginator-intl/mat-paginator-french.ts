import { MatPaginatorIntl } from '@angular/material/paginator';


export function matPaginatorFrench() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.nextPageLabel = 'Page suivante';
  paginatorIntl.previousPageLabel = 'Page précédente';
  paginatorIntl.lastPageLabel = 'Dernière page';
  paginatorIntl.firstPageLabel = 'Première page';
  paginatorIntl.itemsPerPageLabel = 'Entrées par page';

  return paginatorIntl;
}
