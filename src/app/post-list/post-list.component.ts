import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PostService } from '../services/post.service';
import { PostItem } from '../models/post-item';
import { ItemType } from '../models/item-type.enum';
import { BehaviorSubject } from 'rxjs';
import { catchError, take, takeWhile } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { AuthService } from '../services/auth.service';
import { ErrorService } from '../services/error.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns: string[];
  dataSource: DataSource;
  selection = new SelectionModel<Partial<PostItem>>(true, []);
  itemType = ItemType;

  private alive = true;

  constructor(public authService: AuthService,
              private postService: PostService,
              private errorService: ErrorService) {
  }

  ngOnInit(): void {
    this.dataSource = new DataSource(this.postService, this.errorService);
    this.dataSource.paginator = this.paginator;
    this.dataSource.loadData();

    this.authService.isAuthorized$().pipe(takeWhile(() => this.alive))
      .subscribe((isLoggedIn: boolean) => {
        if (isLoggedIn) {
          this.displayedColumns = ['select', 'date', 'grade', 'name', 'letter', 'package', 'delete'];
        } else {
          this.displayedColumns = ['date', 'grade', 'name', 'letter', 'package'];
        }
        this.sort.sortChange.pipe(takeWhile(() => this.alive))
          .subscribe((event: { active: string, direction: 'asc' | 'desc' }) => this.dataSource.sortPostData(event));
      });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  deleteItem(id: number): void {
    this.postService.delete$(id).pipe(take(1)).subscribe(success => {
      if (success) {
        this.dataSource.loadData();
      }
      // TODO: add error handling
    });
  }

  applyFilter(event: Event): void {
    this.dataSource.filterData((event.target as HTMLInputElement).value);
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.displayedItems.length;
    return numSelected === numRows;
  }

  masterToggle(): void {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.displayedItems.forEach(row => this.selection.select(row));
  }
}

// noinspection UnterminatedStatementJS,UnterminatedStatementJS,UnterminatedStatementJS
export class DataSource extends MatTableDataSource<PostItem> {

  postSubject$ = new BehaviorSubject<PostItem[]>([]);
  postItems: PostItem[] = [];
  displayedItems: PostItem[] = [];

  constructor(private postService: PostService,
              private errorService: ErrorService) {
    super();
  }

  filterData(filterValue: string): void {
    this.displayedItems = this.postItems.filter(
      (item: PostItem) => item.name.trim().toLowerCase().includes(filterValue.toLowerCase())
    );
    this.postSubject$.next(this.displayedItems);
    this.updatePaginator();
  }

  loadData(): void {
    this.postService.getAll()
      .pipe(catchError(error => this.errorService.handle(error)))
      .subscribe(items => {
        this.postItems = items;
        this.displayedItems = items;
        setTimeout(() => {
          this.paginator.length = items.length;
          this.postSubject$.next(items.slice(0, this.paginator.pageSize));
        }, 250);
      });
  }

  page(event: PageEvent): void {
    const index = event.pageSize * event.pageIndex;
    this.postSubject$.next(this.displayedItems.slice(index, index + event.pageSize));
  }

  sortPostData(event: { active: string, direction: 'asc' | 'desc' }): void {
    this.displayedItems = this.displayedItems.sort((a: PostItem, b: PostItem) =>  {
      if (a[event.active] < b[event.active]) { return -1; }
      if (a[event.active] > b[event.active]) { return 1; }
      return 0;
    });
    if (event.direction === 'desc') {
      this.displayedItems.reverse();
    }
    this.postSubject$.next(this.displayedItems);
    this.updatePaginator();
  }

  filterPredicate = (data: PostItem, filter: string): boolean => {
    return data.name.toLowerCase().includes(filter);
  }

  connect(): BehaviorSubject<PostItem[]> {
    return this.postSubject$;
  }

  disconnect(): void {
    return this.postSubject$.complete();
  }

  private  updatePaginator() {
    this.page({
      previousPageIndex: 0,
      length: this.displayedItems.length,
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize
    });
    this.paginator.length = this.displayedItems.length;
  }

}
