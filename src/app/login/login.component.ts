import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  login(): void {
    this.authService.login$(this.form.controls['username'].value, this.form.controls['password'].value)
      .subscribe((token) => {
        if (this.authService.isAuthorized()) {
          this.router.navigate(['/admin']);
        }
      }, () => {
        this.form.controls['password'].setErrors({ 'login-error': 'login error' });
      });
  }

  recoverPassword(): void {
    // TODO: implement
  }

}
