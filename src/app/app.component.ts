import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

// @ts-ignore
import packageIcon from '!!raw-loader!@mdi/svg/svg/package.svg';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'milpost';

  constructor(public authService: AuthService,
              private iconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer) {
    iconRegistry
      .addSvgIconLiteral('package', sanitizer.bypassSecurityTrustHtml(packageIcon));
  }

  public changeLanguage(code: string) {
    localStorage.setItem('locale', code);
    window.location.reload();
  }

  public getLanguage(): string {
    return localStorage.getItem('locale');
  }

  logout(): void {
    this.authService.logout();
  }
}
