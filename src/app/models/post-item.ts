import { ItemType } from './item-type.enum';

export class PostItem {
  id: number;
  grade: string;
  name: string;
  company: string;
  section: number;
  itemType: ItemType;
  timeStamp: string;
}
