import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentAdminToken$: BehaviorSubject<string>;

  constructor(private router: Router,
              private http: HttpClient) {
    this.currentAdminToken$ = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('adminToken')));
  }

  isAuthorized(): boolean {
    return this.currentAdminToken$.value != null;
  }

  isAuthorized$(): Observable<boolean> {
    return this.currentAdminToken$.pipe(map(token => !!token));
  }

  getToken(): string {
    return this.currentAdminToken$.value;
  }

  login$(username: string, password: string): Observable<string> {
    const headers = new HttpHeaders({
      Authorization: `Basic ${window.btoa(username + ':' + password)}`
    });

    return this.http.post(`${environment.apiUrl}/login`, null, { headers })
      .pipe(catchError((error) => throwError(error)), map((token: string) => {
        localStorage.setItem('adminToken', JSON.stringify(token));
        this.currentAdminToken$.next(token);
        return token;
      }));
  }

  logout(): void {
    localStorage.removeItem('adminToken');
    this.currentAdminToken$.next(null);
    this.router.navigate(['/']);
  }

}
