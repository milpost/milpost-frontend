import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostItem } from '../models/post-item';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { ErrorService } from './error.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient,
              private errorService: ErrorService) { }

  delete$(id: number): Observable<boolean> {
    return this.http.delete<void>(`${environment.apiUrl}/post/${id}`)
      .pipe(
        catchError(error => this.errorService.handle(error)),
        map(() => {
          return true;
        })
      );
  }

  add(item: Partial<PostItem>): Observable<PostItem> {
    return this.http.post(`${environment.apiUrl}/post`, item)
      .pipe(
        catchError(error => this.errorService.handle(error)),
        map((response) => {
          return response.result.post;
        })
      );
  }

  getAll(): Observable<PostItem[]> {
    return this.http.get(`${environment.apiUrl}/post`)
      .pipe(
        catchError(error => this.errorService.handle(error)),
        map((response) => {
          return response.result.post;
        })
      );
  }

}
