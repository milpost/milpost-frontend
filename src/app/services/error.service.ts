import { Injectable } from '@angular/core';
import { ObservableInput, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  handle(msg: any): ObservableInput<any> {
    console.error('Error occurred: ' + msg);
    return throwError(msg);
  }
}
