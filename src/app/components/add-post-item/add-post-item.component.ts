import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PostItem } from '../../models/post-item';
import { ItemType } from '../../models/item-type.enum';
import { autocompleteOrNullValidator } from '../../validators/autocompleteValidator';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'app-add-post-item',
  templateUrl: './add-post-item.component.html',
  styleUrls: ['./add-post-item.component.scss']
})
export class AddPostItemComponent implements OnInit {

  // options for autocomplete
  companyOptions: number[] = [1, 2];
  sectionOptions: number[] = [1, 2, 3, 4, 5, 6];
  gradeOptions: string[] = ['Rekr', 'Wm', 'Hptfw', 'Hptm'];

  form = new FormGroup({
    company: new FormControl('', [autocompleteOrNullValidator(this.companyOptions)]),
    section: new FormControl('', [autocompleteOrNullValidator(this.sectionOptions)]),
    grade: new FormControl('', [autocompleteOrNullValidator(this.gradeOptions)]),
    name: new FormControl('', [Validators.required]),
    letters: new FormControl(''),
    packages: new FormControl('')
  });

  isPostAssociated = true;

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(() => this.isPostAssociated = true);
  }

  addItem(): void {
    if (this.form.controls['letters'].value === '' && this.form.controls['packages'].value === '') {
      this.isPostAssociated = false;
      return;
    }

    if (this.form.invalid) {
      return;
    }

    const item: Partial<PostItem> = {
      name: this.form.controls['name'].value,
      itemType: ItemType.Letter,
      grade: this.form.controls['grade'].value,
      company: this.form.controls['company'].value,
      section: this.form.controls['section'].value
    };

    this.postService.add(item);
  }

}
