import { AbstractControl, ValidatorFn } from '@angular/forms';

export function autocompleteOrNullValidator(validOptions: (string | number)[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value === '') {
      return null;
    }

    if (validOptions.indexOf(control.value) !== -1) {
      return null;
    }
    return { invalidOption: { value: control.value } };
  };
}
