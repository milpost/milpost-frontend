import { Injectable } from '@angular/core';
import { TimeSeries } from '../models/time-series';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { catchError, map, take } from 'rxjs/operators';
import { ErrorService } from '../../services/error.service';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private http: HttpClient,
              private errorService: ErrorService) { }

  getPostPerDate$(): Observable<TimeSeries[]> {
    // return this.http.get(`${environment.apiUrl}/post/per-day`)
    //   .pipe(take(1), catchError(error => this.errorService.handle(error)), map((response) => {
    //     return response.result.data;
    //   }));
    return of([
      {
        name: 'Germany',
        series: [
          {
            name: '2010',
            value: 7300000
          },
          {
            name: '2011',
            value: 8940000
          },
          {
            name: '2012',
            value: 7300000
          },
          {
            name: '2013',
            value: 8940000
          },
          {
            name: '2014',
            value: 7300000
          },
          {
            name: '2015',
            value: 8940000
          },
          {
            name: '2016',
            value: 7300000
          },
          {
            name: '2017',
            value: 8940000
          }
        ]
      },
      {
        name: 'USA',
        series: [
          {
            name: '2010',
            value: 7870000
          },
          {
            name: '2011',
            value: 8270000
          },
          {
            name: '2012',
            value: 7870000
          },
          {
            name: '2013',
            value: 8270000
          },
          {
            name: '2014',
            value: 7870000
          },
          {
            name: '2015',
            value: 8270000
          },
          {
            name: '2016',
            value: 7870000
          },
          {
            name: '2017',
            value: 8270000
          }
        ]
      }
    ]);
  }
}
