export class NameValuePair {
  name: string | number | Date;
  value: string | number | Date;
}
