import { NameValuePair } from './name-value-pair';

export class TimeSeries {
  name: string;
  series: NameValuePair[];
}
