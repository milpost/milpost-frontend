import { Component, OnInit } from '@angular/core';
import { TimeSeries } from './models/time-series';
import { StatisticsService } from './services/statistics.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  data: TimeSeries[];

  constructor(private statisticsService: StatisticsService) {
  }

  ngOnInit(): void {
    this.statisticsService.getPostPerDate$().pipe(take(1)).subscribe((data: TimeSeries[]) => {
      this.data = data;
    });
  }

}
